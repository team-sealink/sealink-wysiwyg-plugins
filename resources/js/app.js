if (!RedactorPlugins) var RedactorPlugins = {};

//Super and Sub buttons
RedactorPlugins.scriptbuttons = function() {
  return {
    init: function() {
      var sup = this.button.add('superscript', 'Superscript');
      this.button.setIcon(sup, 'x²');
      var sub = this.button.add('subscript', 'Subscript');
      this.button.setIcon(sub, 'x₂');

      this.button.addCallback(sup, this.scriptbuttons.formatSup);
      this.button.addCallback(sub, this.scriptbuttons.formatSub);
    },
    formatSup: function() {
      this.inline.format('sup');
    },
    formatSub: function() {
      this.inline.format('sub');
    }
  };
};

//Left Centre and Right buttons
RedactorPlugins.alignment = function()   {
  return {
    init: function() {
      var that = this;

      var left = this.button.add('left-aligned', 'Left Aligned');
      this.button.addCallback(left, that.alignment.setLeft);
      this.button.setIcon(left, '<i class="icon-sl-left">&nbsp;</i>');
      var center = this.button.add('centre-aligned', 'Centre Aligned');
      this.button.addCallback(center, that.alignment.setCenter);
      this.button.setIcon(center, '<i class="icon-sl-center">&nbsp;</i>');
      var right = this.button.add('right-aligned', 'Right Aligned');
      this.button.addCallback(right, that.alignment.setRight);
      this.button.setIcon(right, '<i class="icon-sl-right">&nbsp;</i>');
    },
    removeAlign: function() {
      this.block.removeClass('text-left');
      this.block.removeClass('text-center');
      this.block.removeClass('text-right');
    },
    setLeft: function() {
      this.buffer.set();
      this.alignment.removeAlign();
      this.block.addClass('text-left');
    },
    setCenter: function() {
      this.buffer.set();
      this.alignment.removeAlign();
      this.block.addClass('text-center');
    },
    setRight: function() {
      this.buffer.set();
      this.alignment.removeAlign();
      this.block.addClass('text-right');
    }
  };
};

//Big and Small buttons
RedactorPlugins.bigsmall = function()   {
  return {
    init: function() {
      var that = this;

      var small = this.button.add('small-text', 'Smaller text');
      this.button.setIcon(small, '<i class="icon-sl-small">&nbsp;</i>');

      var sdropdown = {};
      sdropdown.block = { title: 'Block', func: that.bigsmall.setSmall };
      sdropdown.selection = { title: 'Selection', func: that.bigsmall.setSmallSelection };
      this.button.addDropdown(small, sdropdown);

      var big = this.button.add('larger-text', 'Larger text');
      this.button.setIcon(big, '<i class="icon-sl-big">&nbsp;</i>');

      var bdropdown = {};
      bdropdown.block = { title: 'Block', func: that.bigsmall.setBig };
      bdropdown.selection = { title: 'Selection', func: that.bigsmall.setBigSelection };
      this.button.addDropdown(big, bdropdown);
    },
    setSmall: function() {
      this.buffer.set();
      this.block.removeClass('text-big');
      this.block.toggleClass('text-small');
    },
    setSmallSelection: function() {
      this.buffer.set();
      this.inline.format('small', 'class', 'text-small');
    },
    setBig: function() {
      this.buffer.set();
      this.block.removeClass('text-small');
      this.block.toggleClass('text-big');
    },
    setBigSelection: function() {
      this.buffer.set();
      this.inline.format('mark', 'class', 'text-big');
    }
  };
};
