<?php
namespace Craft;


class SLWysiwygPlugin extends BasePlugin
{

  public function getName()
  {
    return Craft::t('SeaLink WYSIWYG plugins');
  }

  public function getVersion()
  {
    return '1.0.0';
  }

  public function getDeveloper()
  {
    return 'Alex Brindal';
  }

  public function getDeveloperUrl()
  {
    return 'http://sealinktravelgroup.com.au/';
  }

  public function hasCpSection()
  {
      return false;
  }

  /* Basic plugin that provides additional Redactor plugins for use */

  function init() {

    // check we have a cp request as we don't want to this js to run anywhere but the cp
    // and while we're at it check for a logged in user as well
    if ( craft()->request->isCpRequest() && craft()->userSession->isLoggedIn() )
    {

      // the includeJsResource method will add a js file to the bottom of the page
      craft()->templates->includeJsResource('slwysiwyg/js/app.js');

      // the includeCssResource method will add a link in the head
      craft()->templates->includeCssResource('slwysiwyg/css/style.css');

    }

  }

}
